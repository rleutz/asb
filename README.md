# Android Studio Brasil Jetpack Compose

## Telegram group
<a href="https://t.me/android_studio_brasil">Android Studio Brasil</a>

## License
GNU GENERAL PUBLIC LICENSE VERSION 3

## Description
Functions, components and widgets for Jetpack Compose.

## Developers
- Rodrigo Leutz - @rleutz
- Wagner Arcieri - @WagnerArcieri


