package br.com.uware.androidstudiobrasiljetpackcompose.widgets.login_form

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Checkbox
import androidx.compose.material3.CheckboxDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp


/**
 * LoginFormCheckBoxRow
 *
 * @param checked MutableState<Boolean> for checked content.
 * @param colors LoginFormColors.kt model config file.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 03/06/2022 - Initial release.
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun LoginFormCheckBoxRow(
    checked: MutableState<Boolean>,
    colors: LoginFormColors = LoginFormColors(),
    titles: LoginFormStrings = LoginFormStrings(),
    privacyOnClick: () -> Unit,
    termsOnClick: () -> Unit
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 8.dp),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center
    ) {
        Checkbox(
            checked = checked.value,
            onCheckedChange = { checked.value = it },
            colors = CheckboxDefaults.colors(
                checkedColor = colors.background,
                uncheckedColor = colors.disabledButton,
                checkmarkColor = colors.color
            )
        )
        Text(text = buildString {
            Text(
                text = buildAnnotatedString {
                    withStyle(
                        style = SpanStyle(
                            fontSize = 14.sp
                        )
                    ) {
                        withStyle(
                            SpanStyle(
                                color = colors.disabledButton
                            )
                        ) {
                            append("${titles.accept} ")
                        }
                    }
                }
            )
            Text(text = buildAnnotatedString {
                withStyle(
                    style = SpanStyle(
                        color = colors.color,
                        fontSize = 14.sp
                    ),
                ) {
                    append(titles.privacyPolicy)
                }
            },
                modifier = Modifier.clickable {
                    privacyOnClick()
                }
            )
            Text(text = buildAnnotatedString {
                withStyle(
                    SpanStyle(
                        color = colors.disabledButton,
                        fontSize = 14.sp
                    )
                ) {
                    append(" ${titles.and} ")
                }
            })
            Text(text = buildAnnotatedString {
                withStyle(
                    SpanStyle(
                        color = colors.color,
                        fontSize = 14.sp
                    )
                ) {
                    append(titles.termsOfUse)
                }
            }, modifier = Modifier.clickable {
                termsOnClick()
            })
            Text(text = buildAnnotatedString {
                withStyle(
                    SpanStyle(
                        color = colors.disabledButton,
                        fontSize = 14.sp
                    )
                ) {
                    append(".")
                }
            })
        }, textAlign = TextAlign.Center)
    }
}

@Preview(showBackground = true)
@Composable
fun LoginFormCheckBoxRowPreview() {
    val check = remember {
        mutableStateOf(false)
    }
    LoginFormCheckBoxRow(check, privacyOnClick = {}, termsOnClick = {})
}