package br.com.uware.androidstudiobrasiljetpackcompose.widgets.login_form

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.sp


/**
 * LoginFormInputText
 * EditText for Jetpack Compose.
 * @param modifier Modifier
 * @param valueState MutableState<String> for value.
 * @param title String for title in InputField.
 * @param color Int of resource color.
 * @param enable Boolean = true
 * @param isSingleLine Boolean for single line = true.
 * @param keyboardType KeyboardType = Text.
 * @param imeAction = ImeAction for keyboard = Next.
 * @param onAction KeyboardAction = Default.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 02/06/2022 - Initial release.
 */
@Composable
fun LoginFormInputText(
    modifier: Modifier = Modifier,
    valueState: MutableState<String>,
    title: String,
    color: Color,
    enable: Boolean = true,
    isSingleLine: Boolean = true,
    keyboardType: KeyboardType = KeyboardType.Email,
    imeAction: ImeAction = ImeAction.Next,
    onAction: KeyboardActions = KeyboardActions.Default,
) {
    OutlinedTextField(
        value = valueState.value, onValueChange = { valueState.value = it },
        label = { Text(text = title) },
        singleLine = isSingleLine,
        textStyle = TextStyle(
            fontSize = 18.sp,
            color = MaterialTheme.colorScheme.onBackground
        ),
        enabled = enable,
        keyboardOptions = KeyboardOptions(keyboardType = keyboardType, imeAction = imeAction),
        keyboardActions = onAction,
        modifier = modifier.fillMaxWidth(),
        colors = TextFieldDefaults.outlinedTextFieldColors(
            focusedBorderColor = color,
            unfocusedBorderColor = color,
            focusedLabelColor = color,
            cursorColor = color
        )
    )
}
