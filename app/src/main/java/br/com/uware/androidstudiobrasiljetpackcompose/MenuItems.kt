package br.com.uware.androidstudiobrasiljetpackcompose

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.*
import br.com.uware.androidstudiobrasiljetpackcompose.widgets.drawer_menu.DrawerMenuItem
import br.com.uware.androidstudiobrasiljetpackcompose.navigation.ASBScreens


/**
 * MenuItems
 *
 * @author Rodrigo Leutz
 * @version 1.0.0 - 31/05/2022 - Initial release.
 */
val drawerMenuItems: ArrayList<DrawerMenuItem> = arrayListOf(
    DrawerMenuItem("Home", ASBScreens.HomeScreen.name, Icons.Rounded.Home),
    DrawerMenuItem(
        "Animation",
        ASBScreens.AnimationScreen.name, Icons.Rounded.Animation
    ),
    DrawerMenuItem(
        "Choice Row",
        ASBScreens.ChoiceRowScreen.name, Icons.Rounded.SelectAll
    ),
    DrawerMenuItem(
        "Drag Drop List",
        ASBScreens.DragDropListScreen.name, Icons.Rounded.DragIndicator
    ),
    DrawerMenuItem(
        "Input Password",
        ASBScreens.InputPasswordFieldScreen.name,
        Icons.Rounded.Password
    ),
    DrawerMenuItem(
        "Login Form",
        ASBScreens.LoginScreen.name, Icons.Rounded.Login
    ),
    DrawerMenuItem(
        "Round Button",
        ASBScreens.RoundButtonScreen.name, Icons.Rounded.SmartButton
    ),
    DrawerMenuItem(
        "Text With Shadow",
        ASBScreens.TextWithShadowScreen.name, Icons.Rounded.TextFormat
    ),
    DrawerMenuItem(
        "Toast",
        ASBScreens.ToastScreen.name, Icons.Rounded.TextFields
    ),
)