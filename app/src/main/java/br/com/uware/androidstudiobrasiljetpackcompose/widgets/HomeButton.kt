package br.com.uware.androidstudiobrasiljetpackcompose.widgets

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp


/**
 * HomeButton
 * Button for Home screen.
 * @param title Int of resource string.
 * @param icon ImageVector of Icons.
 * @param navigate ASBScreens name of destination.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 30/05/2022 - Initial release.
 */
@Composable
fun HomeButton(
    modifier: Modifier = Modifier,
    title: String,
    icon: ImageVector,
    backgroundColor: Color = Color.DarkGray,
    contentColor: Color = Color.White,
    navigate: () -> Unit,
){
    Button(
        modifier = modifier
            .fillMaxWidth()
            .padding(8.dp),
        onClick = {
            navigate()
        },
        colors = ButtonDefaults.buttonColors(
            containerColor = backgroundColor,
            contentColor = contentColor
        ),
        elevation = ButtonDefaults.buttonElevation(
            defaultElevation = 16.dp,
            pressedElevation = 0.dp
        )
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            Icon(
                modifier = Modifier.padding(end = 8.dp),
                imageVector = icon,
                contentDescription = title
            )
            Text(text = title)
        }
    }
}