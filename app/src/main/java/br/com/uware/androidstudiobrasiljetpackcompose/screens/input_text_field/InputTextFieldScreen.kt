package br.com.uware.androidstudiobrasiljetpackcompose.screens.input_text_field

import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview


/**
 * InputTextFieldScreen
 *
 * @author Rodrigo Leutz
 * @version 1.0.0 - 01/06/2022 - Initial release.
 */
@Composable
fun InputTextFieldScreen(){

}

@Preview(showBackground = true)
@Composable
fun InputTextFieldScreenPreview(){
    InputTextFieldScreen()
}