package br.com.uware.androidstudiobrasiljetpackcompose.widgets.login_form


/**
 * LoginFormTitles
 * Strings for LoginForm.
 *
 * @author Rodrigo Leutz
 * @version 1.0.0 - 02/06/2022 - Initial release.
 */
data class LoginFormStrings(
    var accept: String = "Accept",
    var and: String = "and",
    var email: String = "Email",
    var forgotPassword: String = "Forgot password?",
    var forgotPasswordAcceptButton: String = "Send",
    var forgotPasswordDenyButton: String = "Cancel",
    var login: String = "Login",
    var name: String = "Name",
    var newUser: String = "New User",
    var loginAcceptButton: String = "Login",
    var loginDenyButton: String = "Cancel",
    var password: String = "Password",
    var passwordConfirm: String = "Confirm Password",
    var passwordConfirmError: String = "Password and Confirm Password not match.",
    var privacyPolicy:String = "Privacy policy",
    var register: String = "Register",
    var registerAcceptButton: String = "Register",
    var registerDenyButton: String = "Cancel",
    var signUp: String = "Sign Up",
    var termsOfUse: String = "Terms of use",
    var checkedError: String = "Need check $privacyPolicy $and $termsOfUse.",
)