package br.com.uware.androidstudiobrasiljetpackcompose.components.layouts

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier


/**
 * CenterColumn
 * Column with centered items.
 * @param content { @Unit } of content in screen.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 01/06/2022 - Initial release.
 */
@Composable
fun CenterColumn(
    content: @Composable () -> Unit = {}
){
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        content()
    }
}