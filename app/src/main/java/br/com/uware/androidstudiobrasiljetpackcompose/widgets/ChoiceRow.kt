package br.com.uware.androidstudiobrasiljetpackcompose.widgets

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp


/**
 * ChoiceRow
 * Row for Accept or Deny choice.
 * @param modifier Modifier for Row.
 * @param buttonModifier Modifier for buttons.
 * @param buttonPadding Padding in dp for buttons.
 * @param buttonElevation ButtonElevation for buttons.
 * @param acceptTitle String title to accept button.
 * @param denyTitle String title to deny button.
 * @param acceptColors ButtonColors for accept button.
 * @param denyColors ButtonColors for deny button.
 * @param acceptEnabled MutableState<Boolean> button accept is enabled.
 * @param denyEnabled MutableState<Boolean> button deny is enabled.
 * @param acceptOnClick { Unit } OnClick event for accept button.
 * @param denyOnClick { Unit } OnClick event for deny button.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 31/05/2022 - Initial release.
 */
@Composable
fun ChoiceRow(
    modifier: Modifier = Modifier,
    buttonModifier: Modifier = Modifier,
    buttonPadding: Dp = 8.dp,
    buttonElevation: ButtonElevation = ButtonDefaults.buttonElevation(
        defaultElevation = 16.dp,
        pressedElevation = 0.dp
    ),
    acceptTitle: String = "",
    denyTitle: String = "",
    acceptColors: ButtonColors = ButtonDefaults.buttonColors(
        containerColor = Color(0xFF176510),
        contentColor = Color(0xFFFFFFFF),
        disabledContainerColor = Color(0xFF363434),
        disabledContentColor = Color(0xFFFFFFFF)
    ),
    denyColors: ButtonColors = ButtonDefaults.buttonColors(
        containerColor = Color(0xFF991818),
        contentColor = Color(0xFFFFFFFF),
        disabledContainerColor = Color(0xFF363434),
        disabledContentColor = Color(0xFFFFFFFF)
    ),
    acceptEnabled: MutableState<Boolean> = remember {
        mutableStateOf(true)
    },
    denyEnabled: MutableState<Boolean> = remember {
        mutableStateOf(true)
    },
    denyOnClick: () -> Unit = {},
    acceptOnClick: () -> Unit = {}
) {
    Row(
        modifier = modifier
            .fillMaxWidth()
            .padding(buttonPadding)
    ) {
        ChoiceButton(
            modifier = buttonModifier
                .weight(1f)
                .fillMaxWidth()
                .padding(end = (buttonPadding / 2)),
            title = acceptTitle,
            colors = acceptColors,
            elevation = buttonElevation,
            enabled = acceptEnabled.value
        ){
            acceptOnClick()
        }
        ChoiceButton(
            modifier = buttonModifier
                .weight(1f)
                .fillMaxWidth()
                .padding(start = (buttonPadding / 2)),
            title = denyTitle,
            colors = denyColors,
            elevation = buttonElevation,
            enabled = denyEnabled.value
        ){
            denyOnClick()
        }
    }
}

/**
 * ChoiceButton
 * Default choice button.
 * @param modifier Modifier.
 * @param title String title.
 * @param colors ButtonColors for button.
 * @param elevation ButtonElevation for button.
 *
 * @param onClick { Unit } OnClick event.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 31/05/2022 - Initial release.
 */
@Composable
fun ChoiceButton(
    modifier: Modifier = Modifier,
    title: String,
    colors: ButtonColors,
    elevation: ButtonElevation,
    enabled: Boolean = true,
    onClick: () -> Unit = {}
) {
    Button(
        modifier = modifier,
        onClick = {
            onClick()
        },
        colors = colors,
        elevation = elevation,
        enabled = enabled
    ) {
        Text(text = title)
    }
}

@Preview(showBackground = true)
@Composable
fun ChoiceRowPreview() {
    ChoiceRow(
        modifier = Modifier.fillMaxWidth(),
        acceptTitle = "Accept",
        denyTitle = "Deny"
    )
}