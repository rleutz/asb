package br.com.uware.androidstudiobrasiljetpackcompose.screens.toast

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import br.com.uware.androidstudiobrasiljetpackcompose.R
import br.com.uware.androidstudiobrasiljetpackcompose.components.inputs.InputTextField
import br.com.uware.androidstudiobrasiljetpackcompose.functions.toastMessage


/**
 * ToastScreen
 * Screen for toastMessage function test.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 30/05/2022 - Initial release.
 */
@Composable
fun ToastScreen(){
    val toastText = remember {
        mutableStateOf("Test")
    }
    val context = LocalContext.current
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(8.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        InputTextField(
            valueState = toastText,
            title = stringResource(id = R.string.toast_screen),
            color = Color(0xFFFFFFFF)
        )
        Button(onClick = {
            context.toastMessage(toastText.value)
        }) {
            Text(text = stringResource(id = R.string.toast_screen))
        }
    }
}

@Preview(showBackground = true)
@Composable
fun ToastScreenPreview(){
    ToastScreen()
}