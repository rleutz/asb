package br.com.uware.androidstudiobrasiljetpackcompose.screens.home

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import br.com.uware.androidstudiobrasiljetpackcompose.MainViewModel
import br.com.uware.androidstudiobrasiljetpackcompose.R
import br.com.uware.androidstudiobrasiljetpackcompose.drawerMenuItems
import br.com.uware.androidstudiobrasiljetpackcompose.navigation.ASBScreens
import br.com.uware.androidstudiobrasiljetpackcompose.widgets.HomeButton


/**
 * HomeScreen
 * Home screen.
 * @param mainViewModel MainViewModel of app.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 30/05/2022 - Initial release.
 */
@Composable
fun HomeScreen(mainViewModel: MainViewModel) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(8.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Box(
            modifier = Modifier.fillMaxSize()
        ) {
            LazyColumn(
                modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                items(drawerMenuItems.sortedBy { it.title }) { item ->
                    if (item.destination != ASBScreens.HomeScreen.name) {
                        HomeButton(
                            title = item.title,
                            icon = item.icon
                        ) {
                            mainViewModel.navHostController.navigate(item.destination)
                        }
                    }
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun HomeScreenPreview() {
    val mainViewModel = MainViewModel()
    HomeScreen(mainViewModel = mainViewModel)
}