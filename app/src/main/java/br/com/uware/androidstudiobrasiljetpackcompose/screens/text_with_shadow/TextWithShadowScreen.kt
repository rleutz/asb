package br.com.uware.androidstudiobrasiljetpackcompose.screens.text_with_shadow

import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import br.com.uware.androidstudiobrasiljetpackcompose.MainViewModel
import br.com.uware.androidstudiobrasiljetpackcompose.components.buttons.BackButton
import br.com.uware.androidstudiobrasiljetpackcompose.components.layouts.CenterColumn
import br.com.uware.androidstudiobrasiljetpackcompose.components.texts.TextWithShadow


/**
 * TextWithShadowScreen
 *
 * @author Rodrigo Leutz
 * @version 1.0.0 - 01/06/2022 - Initial release.
 */
@Composable
fun TextWithShadowScreen(
    mainViewModel: MainViewModel
){
    CenterColumn {
        TextWithShadow(
            text = "Android Studio Brasil",
            color = Color.Blue,
            style = MaterialTheme.typography.displaySmall,
            fontWeight = FontWeight.Bold
        )
        BackButton(navController = mainViewModel.navHostController)
    }
}

@Preview(showBackground = true)
@Composable
fun TextWithShadowScreenPreview(){
    val mainViewModel = MainViewModel()
    TextWithShadowScreen(mainViewModel = mainViewModel)
}