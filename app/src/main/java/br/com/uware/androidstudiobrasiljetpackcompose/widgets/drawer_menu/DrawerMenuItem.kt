package br.com.uware.androidstudiobrasiljetpackcompose.widgets.drawer_menu

import androidx.compose.ui.graphics.vector.ImageVector

/**
 * DrawerItem
 * Navigation drawer item model.
 * @param title Title of menu.
 * @param destination String of route in ASBScreens.
 * @param icon ImageVector of icon.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 31/05/2022 - Initial release.
 */
data class DrawerMenuItem(
    val title: String,
    val destination: String,
    val icon: ImageVector
)
