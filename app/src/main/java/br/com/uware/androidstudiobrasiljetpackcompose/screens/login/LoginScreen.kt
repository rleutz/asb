package br.com.uware.androidstudiobrasiljetpackcompose.screens.login

import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Android
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import br.com.uware.androidstudiobrasiljetpackcompose.MainViewModel
import br.com.uware.androidstudiobrasiljetpackcompose.R
import br.com.uware.androidstudiobrasiljetpackcompose.functions.toastMessage
import br.com.uware.androidstudiobrasiljetpackcompose.widgets.login_form.LoginForm
import br.com.uware.androidstudiobrasiljetpackcompose.widgets.login_form.LoginFormColors


/**
 * LoginScreen
 *
 * @author Rodrigo Leutz
 * @version 1.0.0 - 02/06/2022 - Initial release.
 */
@Composable
fun LoginScreen(mainViewModel: MainViewModel) {
    val context = LocalContext.current
    val colors = LoginFormColors()
    colors.color = colorResource(id = R.color.green_500)
    LoginForm(
        headerContent = {
            Row(
                modifier = Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Center
            ) {
                Icon(
                    imageVector = Icons.Rounded.Android,
                    contentDescription = "Android",
                    modifier = Modifier.size(80.dp)
                )
                Text(text = "Logo", style = MaterialTheme.typography.displayMedium)
            }
        },
        colors = colors,
        modifier = Modifier.padding(32.dp),
        policyPrivacyOnClick = {
            context.toastMessage("Clicked in policy privacy.")
        },
        termsOfUseOnClick = {
            context.toastMessage("Clicked in terms of use.")
        },
        forgotPasswordOnClick = { email ->
            "Forgot email: $email"
        },
        loginAccept = { email, password ->
            context.toastMessage("Email: $email\nPassword: $password")
            "In snackbar - Email: $email\nPassword: $password"
        },
        loginDeny = {
            mainViewModel.navHostController.popBackStack()
        },
        registerAccept = { name, email, password ->
            context.toastMessage("Name: $name\nEmail: $email\nPassword: $password")
            "In snackbar - Name: $name\nEmail: $email\nPassword: $password"
        }
    )

}

@Preview(showBackground = true)
@Composable
fun LoginScreenPreview() {
    val mainViewModel = MainViewModel()
    LoginScreen(mainViewModel)
}