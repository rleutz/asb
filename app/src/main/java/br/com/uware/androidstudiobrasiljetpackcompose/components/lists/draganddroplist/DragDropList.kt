package br.com.uware.androidstudiobrasiljetpackcompose.components.lists.draganddroplist

import android.util.Log
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.detectDragGesturesAfterLongPress
import androidx.compose.foundation.gestures.scrollBy
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.input.pointer.consumeAllChanges
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

/**
 * DragAndDropList
 *
 * Composable Lazy List with implementation for
 * reordering the Items in the List by Drag and Drop
 *
 * @param modifier Modifier to be overwritten. Avoid overriding .pointerInput modifier.
 * @param isItemComposable A Boolean to switch between a pre-defined List or a custom Composable List.
 * @param composableItemsList A MutableList of Composable to be settled as a drag and Drop Item of the Lazy List. Consider using an ArrayList<@Composable () -> Unit>.
 * @param itemText A List of Strings with the items texts.
 * @param itemFontSize The TextUnit value for the Item text. Default = 16.sp
 * @param itemFontFamily The FontFamily value for the Item text. Default = FontFamily.Serif
 * @param itemCornerRadius The Unit in Dp for RoundCornerShape value of item. Default = 8.dp
 * @param itemPadding The Unit in Dp for the item padding modifier value. Default = 20.dp
 * @param itemSpacerHeight The Unit in Dp for the height value of the Spacer between items. Default = Dp = 10.dp
 * @param itemBackgroundColor The Color for the background of the item. Default = Color(0xFFFFFFFF) (White)
 * @param itemWidthFraction A Float for the width fraction that items should occupy in the Lazy List. Default = 1f (fill max)
 * @param onClick The Method invoked on the Button Click listener. Default = { }
 *
 *
 *
 *
 * @author Wagner Arcieri
 * @version 1.0.0 - 01/06/2022 - Initial release.
 * @version 1.0.1 - 01/06/2022 - Added ability to use custom Composable as Items.
 * @version 1.0.2 - 01/06/2022 - added list of composes for preview, syntax correction
 */
@Composable
fun DragDropList(
    modifier: Modifier = Modifier,
    isItemComposable: Boolean = false,
    composableItemsList: MutableList<@Composable () -> Unit > = emptyList<@Composable () -> Unit>().toMutableList(),
    itemText: List<String> = emptyList(),
    itemFontSize: TextUnit = 16.sp,
    itemFontFamily: FontFamily = FontFamily.Serif,
    itemCornerRadius: Dp = 8.dp,
    itemPadding: Dp = 20.dp,
    itemSpacerHeight: Dp = 10.dp,
    itemBackgroundColor: Color = Color(0xFFFFFFFF),
    itemWidthFraction: Float = 1f,
    onClick: () -> Unit = { }
) {

    val scope = rememberCoroutineScope()

    var overScrollJob by remember { mutableStateOf<Job?>(null) }

    val dragDropListState =
        rememberDragDropListState(
        onMove = { fromIndex, toIndex -> listOfItems.move(fromIndex, toIndex)}
        )

    LazyColumn(
        modifier = modifier
            .pointerInput(Unit) {
                detectDragGesturesAfterLongPress(
                    onDrag = { change, offset ->
                        change.consumeAllChanges()
                        dragDropListState.onDrag(offset = offset)

                        if (overScrollJob?.isActive == true)
                            return@detectDragGesturesAfterLongPress

                        dragDropListState
                            .checkForOverScroll()
                            .takeIf { it != 0f }
                            ?.let {
                                overScrollJob = scope.launch {
                                    dragDropListState.lazyListState.scrollBy(it)
                                }
                            } ?: kotlin.run { overScrollJob?.cancel() }
                    },
                    onDragStart = { offset -> dragDropListState.onDragStart(offset) },
                    onDragEnd = { dragDropListState.onDragInterrupted() },
                    onDragCancel = { dragDropListState.onDragInterrupted() }
                )
            }
            .fillMaxSize()
            .padding(top = 10.dp, start = 10.dp, end = 10.dp),
        state = dragDropListState.lazyListState
    ) {

        if (isItemComposable) {
            composableItemsList.forEachIndexed { index, item ->
                item {
                    composableItemsList[index].invoke()
                }
            }
        } else {
            itemsIndexed(itemText) { index, itemText ->
                Column(
                    modifier = Modifier
                        .composed {
                            val offsetOrNull = dragDropListState.elementDisplacement.takeIf {
                                index == dragDropListState.currentIndexOfDraggedItem
                            }
                            Modifier.graphicsLayer {
                                translationY = offsetOrNull ?: 0f
                            }
                        }
                        .background(
                            itemBackgroundColor,
                            shape = RoundedCornerShape(itemCornerRadius)
                        )
                        .fillMaxWidth(itemWidthFraction)
                        .padding(itemPadding)
                        .clickable { onClick.invoke() }
                ) {
                    Text(
                        text = itemText,
                        fontSize = itemFontSize,
                        fontFamily = itemFontFamily
                    )
                }

                Spacer(modifier = Modifier.height(itemSpacerHeight))
            }
        }



    }
}

@Preview
@Composable
fun DragAndDropListPreview(){

    /** Preview WITH Composable Items*/
    Surface(color = MaterialTheme.colorScheme.background) {
        Column(
            modifier = Modifier.fillMaxSize()
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(55.dp)
                    .background(MaterialTheme.colorScheme.surfaceVariant),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                Text(
                    text = "Drag & Drop List",
                    fontSize = 20.sp,
                    fontWeight = FontWeight.Bold,
                    color = MaterialTheme.colorScheme.onSurfaceVariant
                )
            }

            DragDropList(
                onClick = { Log.d("TEST", "CLICKED !!!")  },
                isItemComposable = true,
                composableItemsList = ListOfComposes()

//                listOf ({ Text(text = "test") }, { Text(text = "test") } ).toMutableList()

            )
        }
    }


    /** Preview WITHOUT Composable Items*/
//    Surface(color = Color(0xFFC0C0C0)) {
//        Column(
//            modifier = Modifier.fillMaxSize()
//        ) {
//            Column(
//                modifier = Modifier
//                    .fillMaxWidth()
//                    .height(55.dp)
//                    .background(MaterialTheme.colorScheme.primary),
//                horizontalAlignment = Alignment.CenterHorizontally,
//                verticalArrangement = Arrangement.Center
//            ) {
//                Text(
//                    text = "Drag & Drop List",
//                    fontSize = 20.sp,
//                    fontWeight = FontWeight.Bold,
//                    color = Color(0xFFFFFFFF)
//                )
//            }
//
//            DragDropList(
//                itemText = listOfItems,
//                onClick = { Log.d("TEST", "CLICKED !!!")  }
//            )
//        }
//    }


}

val arrayListOfComposes  = ListOfComposes()

fun ListOfComposes(): MutableList<@Composable () -> Unit> {
    var arrayList: MutableList<@Composable () -> Unit> = emptyArray<@Composable () -> Unit>().toMutableList()

    repeat(15){
        arrayList.add { PreviewCard(count = it) }
    }

    return arrayList
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun PreviewCard(count: Int) {
    Card(
        modifier = Modifier
            .padding(5.dp)
            .fillMaxWidth()
            .height(60.dp)
            .clickable {
                Log.d("TEST", "PreviewCard: CLICKED !!")
            },
        border = BorderStroke(2.dp, MaterialTheme.colorScheme.inversePrimary),
        elevation = CardDefaults.cardElevation(4.dp),
        colors = CardDefaults.cardColors(MaterialTheme.colorScheme.primaryContainer),
    ) {
        Row(
            modifier = Modifier
                .fillMaxSize(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            Text(
                text = "custom card $count",
                fontSize = 15.sp,
                fontWeight = FontWeight.Bold,
                color = MaterialTheme.colorScheme.onPrimaryContainer
            )
        }

    }
}

/** Dummy Data List sample for preview */
val listOfItems = listOf(
    "List Item 1",
    "List Item 2",
    "List Item 3",
    "List Item 4",
    "List Item 5",
    "List Item 6",
    "List Item 7",
    "List Item 8",
    "List Item 9",
    "List Item 10",
    "List Item 11",
    "List Item 12",
    "List Item 13",
    "List Item 14",
    "List Item 15",
    "List Item 16",
    "List Item 17",
    "List Item 18",
    "List Item 19",
    "List Item 20"
).toMutableStateList()

