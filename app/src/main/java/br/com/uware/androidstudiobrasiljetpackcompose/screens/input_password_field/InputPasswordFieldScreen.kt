package br.com.uware.androidstudiobrasiljetpackcompose.screens.input_password_field

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import br.com.uware.androidstudiobrasiljetpackcompose.MainViewModel
import br.com.uware.androidstudiobrasiljetpackcompose.components.inputs.InputPasswordField


/**
 * InputPasswordFieldScreen
 * Input password field screen.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 30/05/2022 - Initial release.
 */
@Composable
fun InputPasswordFieldScreen(mainViewModel: MainViewModel) {
    val passwordVisibility = remember {
        mutableStateOf(false)
    }
    val password = remember {
        mutableStateOf("")
    }
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        InputPasswordField(
            passwordState = password,
            title = "Password",
            passwordVisibility = passwordVisibility
        )
        OutlinedButton(
            onClick = {
                mainViewModel.navHostController.popBackStack()
            },
            modifier = Modifier.padding(16.dp),
            elevation = ButtonDefaults.buttonElevation(
                defaultElevation = 8.dp,
                pressedElevation = 0.dp
            )
        ) {
            Text(text = "Back")
        }
    }
}

@Preview(showBackground = true)
@Composable
fun InputPasswordFieldPreview(){
    val mainViewModel = MainViewModel()
    InputPasswordFieldScreen(mainViewModel = mainViewModel)
}