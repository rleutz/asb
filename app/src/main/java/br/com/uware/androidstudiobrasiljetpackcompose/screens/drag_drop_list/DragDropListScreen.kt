package br.com.uware.androidstudiobrasiljetpackcompose.screens.drag_drop_list

import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import br.com.uware.androidstudiobrasiljetpackcompose.MainViewModel


/**
 * DragDropListScreen
 *
 * @version 1.0.0 - 01/06/2022 - Initial release.
 */
@Composable
fun DragDropListScreen(
    mainViewModel: MainViewModel
) {

}

@Preview(showBackground = true)
@Composable
fun DragDropListScreenPreview(){
    val mainViewModel = MainViewModel()
    DragDropListScreen(mainViewModel = mainViewModel)
}