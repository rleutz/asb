package br.com.uware.androidstudiobrasiljetpackcompose.widgets.login_form

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Visibility
import androidx.compose.material.icons.rounded.VisibilityOff
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.sp

/**
 * LoginFormInputText
 * @param modifier Modifier
 * @param passwordState MutableState<String> for password.
 * @param title String for title.
 * @param passwordVisibility MutableState<Boolean> for password visibility.
 * @param enabled Boolean = true
 * @param color Color for input password.
 * @param trailingIconColor Color for trailing icon.
 * @param imeAction = ImeAction for keyboard = Next.
 * @param onAction KeyboardAction = Default.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 13/05/2022 - Initial release.
 */
@Composable
fun LoginFormInputPassword(
    passwordState: MutableState<String>,
    title: String,
    passwordVisibility: MutableState<Boolean>,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    color: Color = Color.Black,
    trailingIconColor: Color = Color.DarkGray,
    imeAction: ImeAction = ImeAction.Done,
    onAction: KeyboardActions = KeyboardActions.Default,
) {
    val visualTransformation = if (passwordVisibility.value) VisualTransformation.None else
        PasswordVisualTransformation()
    OutlinedTextField(
        value = passwordState.value,
        onValueChange = {
            passwordState.value = it
        },
        label = { Text(text = title) },
        singleLine = true,
        textStyle = TextStyle(fontSize = 18.sp, color = MaterialTheme.colorScheme.onBackground),
        modifier = modifier
            .fillMaxWidth(),
        enabled = enabled,
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Password,
            imeAction = imeAction
        ),
        visualTransformation = visualTransformation,
        trailingIcon = {
            val visible = passwordVisibility.value
            IconButton(
                onClick = {
                    passwordVisibility.value = !visible
                }
            ) {
                Icon(
                    imageVector = if(visible) Icons.Rounded.VisibilityOff
                    else Icons.Rounded.Visibility,
                    contentDescription = "Password Visibility"
                )
            }
        },
        keyboardActions = onAction,
        colors = TextFieldDefaults.outlinedTextFieldColors(
            focusedBorderColor = color,
            unfocusedBorderColor = color,
            focusedLabelColor = color,
            cursorColor = color,
            focusedTrailingIconColor = trailingIconColor,
            disabledTrailingIconColor = trailingIconColor
        )
    )
}