package br.com.uware.androidstudiobrasiljetpackcompose.components.layouts

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color


/**
 * DrawerScaffold
 * Scaffold for navigation drawer and top bar.
 * @param topBar { Unit } TopBar for app.
 * @param colorLight Color light titles.
 * @param backgroundLight Color light background.
 * @param content { Unit } Content in screen.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 30/05/2022 - Initial release.
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DrawerScaffold(
    topBar: @Composable () -> Unit = {},
    colorLight: Color = Color.Black,
    backgroundLight: Color = Color.LightGray,
    content: @Composable () -> Unit = {}
){
    Scaffold(
        modifier = Modifier.fillMaxSize(),
        topBar = {
            topBar()
        },
        contentColor = colorLight,
        containerColor = backgroundLight
    ) { innerPadding ->
        Column(
            modifier = Modifier.padding(innerPadding)
        ) {
            content()
        }
    }
}