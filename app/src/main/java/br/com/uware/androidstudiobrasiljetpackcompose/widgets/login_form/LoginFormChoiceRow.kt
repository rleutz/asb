package br.com.uware.androidstudiobrasiljetpackcompose.widgets.login_form

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp


/**
 * LoginFormChoiceRow
 * Login choice row for login form.
 * @param modifier Modifier for Row.
 * @param isLogin Boolean if is login or register.
 * @param titles LoginFormTitles.kt data class config file.
 * @param colors LoginFormColors.kt data class config file.
 * @param elevation Dp for button elevation.
 * @param internalPadding Dp of padding between buttons.
 * @param acceptEnabled Boolean if accept button is enabled.
 * @param denyEnabled Boolean if deny button is enabled.
 * @param denyOnClick { Unit } executes after deny button click.
 * @param acceptOnClick { Unit } executes after accept button click.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 03/06/2022 - Initial release.
 */
@Composable
fun LoginFormChoiceRow(
    modifier: Modifier = Modifier,
    isLogin: Boolean = true,
    isForgot: Boolean = false,
    titles: LoginFormStrings = LoginFormStrings(),
    colors: LoginFormColors = LoginFormColors(),
    elevation: Dp = 16.dp,
    internalPadding: Dp = 16.dp,
    acceptEnabled: Boolean = true,
    denyEnabled: Boolean = true,
    denyOnClick: () -> Unit = {},
    acceptOnClick: () -> Unit = {}
) {
    Row(
        modifier = modifier.fillMaxWidth()
    ) {
        LoginFormChoiceButton(
            modifier = Modifier
                .weight(1f)
                .padding(end = internalPadding / 2),
            title = if(isForgot && isLogin) titles.forgotPasswordAcceptButton
            else if (isLogin) titles.loginAcceptButton else titles.registerAcceptButton,
            colors = ButtonDefaults.buttonColors(
                containerColor = colors.acceptButton,
                contentColor = colors.acceptButtonText,
                disabledContainerColor = colors.disabledButton,
                disabledContentColor = colors.disabledButtonText
            ),
            elevation = ButtonDefaults.buttonElevation(
                defaultElevation = elevation,
                pressedElevation = 0.dp,
                disabledElevation = 0.dp
            ),
            onClick = {
                acceptOnClick()
            },
            enabled = acceptEnabled
        )
        LoginFormChoiceButton(
            modifier = Modifier
                .weight(1f)
                .padding(start = internalPadding / 2),
            title = if (isForgot && isLogin) titles.forgotPasswordDenyButton
            else if (isLogin) titles.loginDenyButton else titles.registerDenyButton,
            colors = ButtonDefaults.buttonColors(
                containerColor = colors.denyButton,
                contentColor = colors.denyButtonText,
                disabledContainerColor = colors.disabledButton,
                disabledContentColor = colors.disabledButtonText
            ),
            elevation = ButtonDefaults.buttonElevation(
                defaultElevation = elevation,
                pressedElevation = 0.dp,
                disabledElevation = 0.dp
            ),
            onClick = {
                denyOnClick()
            },
            enabled = denyEnabled
        )
    }
}

/**
 * LoginFormChoiceButton
 * Default choice button.
 * @param modifier Modifier.
 * @param title String title.
 * @param colors ButtonColors for button.
 * @param elevation ButtonElevation for button.
 * @param onClick { Unit } OnClick event.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 31/05/2022 - Initial release.
 */
@Composable
fun LoginFormChoiceButton(
    modifier: Modifier = Modifier,
    title: String,
    colors: ButtonColors,
    elevation: ButtonElevation,
    enabled: Boolean = true,
    onClick: () -> Unit = {}
) {
    Button(
        modifier = modifier,
        onClick = {
            onClick()
        },
        colors = colors,
        elevation = elevation,
        enabled = enabled
    ) {
        Text(text = title)
    }
}

@Preview(showBackground = true)
@Composable
fun LoginFormChoiceRowPreview() {
    LoginFormChoiceRow()
}