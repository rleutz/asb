package br.com.uware.androidstudiobrasiljetpackcompose.navigation


/**
 * ASBScreens
 * Screens for app.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 30/05/2022 - Initial release.
 */
enum class ASBScreens {
    AnimationScreen,
    ChoiceRowScreen,
    DragDropListScreen,
    HomeScreen,
    InputPasswordFieldScreen,
    InputTextFieldScreen,
    LoginScreen,
    RoundButtonScreen,
    SplashScreen,
    TestScreen,
    TextWithShadowScreen,
    ToastScreen;
    companion object {
        fun fromRoute(route: String?): ASBScreens =
            when(route?.substringBefore("/")){
                AnimationScreen.name -> AnimationScreen
                ChoiceRowScreen.name -> ChoiceRowScreen
                DragDropListScreen.name -> DragDropListScreen
                HomeScreen.name -> HomeScreen
                InputPasswordFieldScreen.name -> InputPasswordFieldScreen
                InputTextFieldScreen.name -> InputTextFieldScreen
                LoginScreen.name -> LoginScreen
                RoundButtonScreen.name -> RoundButtonScreen
                SplashScreen.name -> SplashScreen
                TestScreen.name -> TestScreen
                TextWithShadowScreen.name -> TextWithShadowScreen
                ToastScreen.name -> ToastScreen
                null -> HomeScreen
                else -> throw IllegalArgumentException("Route $route not found.")
            }
    }
}