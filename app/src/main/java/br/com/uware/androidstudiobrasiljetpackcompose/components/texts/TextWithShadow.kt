package br.com.uware.androidstudiobrasiljetpackcompose.components.texts

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.offset
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp


/**
 * TextWithShadow
 * Text with shadow.
 * @param text String of text.
 * @param modifier Modifier.
 * @param color Color for text.
 * @param shadowColor Color for shadow.
 * @param style Style for text.
 * @param fontWeight FontWeight for text.
 * @param textAlign TextAlign for text.
 * @param alpha Alpha of shadow.
 * @param offset Offset of shadow.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 01/06/2022 - Initial release.
 */
@Composable
fun TextWithShadow(
    text: String,
    modifier: Modifier = Modifier,
    color: Color = Color.Black,
    shadowColor: Color = Color.DarkGray,
    style: TextStyle = MaterialTheme.typography.bodySmall,
    fontWeight: FontWeight,
    textAlign: TextAlign = TextAlign.Center,
    alpha: Float = 0.65f,
    offset: Dp = 2.dp
) {
    Box {
        Text(
            text = text,
            color = shadowColor,
            style = style,
            modifier = modifier
                .offset(
                    x = offset,
                    y = offset
                )
                .alpha(alpha),
            fontWeight = fontWeight,
            textAlign = textAlign
        )
        Text(
            text = text,
            color = color,
            style = style,
            modifier = modifier,
            fontWeight = fontWeight,
            textAlign = textAlign
        )
    }
}

@Preview(showBackground = true)
@Composable
fun TextWithShadowPreview(){
    TextWithShadow(
        text = "Olá Mundo!",
        color = Color.Black,
        shadowColor = Color.Green,
        style = MaterialTheme.typography.displaySmall,
        fontWeight = FontWeight.Bold
    )
}