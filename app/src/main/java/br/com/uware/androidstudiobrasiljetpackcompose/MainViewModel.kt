package br.com.uware.androidstudiobrasiljetpackcompose

import androidx.compose.material3.DrawerState
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.lifecycle.ViewModel
import androidx.navigation.NavHostController
import kotlinx.coroutines.CoroutineScope


/**
 * MainViewModel
 *
 * @author Rodrigo Leutz
 * @version 1.0.0 - 30/05/2022 - Initial release.
 */
class MainViewModel: ViewModel() {
    // NavHostController
    lateinit var navHostController: NavHostController

    // Navigation Drawer State
    @OptIn(ExperimentalMaterial3Api::class)
    lateinit var drawerState: DrawerState

    // CoroutineScope
    lateinit var coroutineScope: CoroutineScope

}