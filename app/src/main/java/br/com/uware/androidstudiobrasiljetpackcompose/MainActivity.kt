package br.com.uware.androidstudiobrasiljetpackcompose

import android.os.Bundle
import android.view.animation.OvershootInterpolator
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import br.com.uware.androidstudiobrasiljetpackcompose.widgets.drawer_menu.DrawerColors
import br.com.uware.androidstudiobrasiljetpackcompose.widgets.drawer_menu.NavigationDrawer
import br.com.uware.androidstudiobrasiljetpackcompose.functions.toastMessage
import br.com.uware.androidstudiobrasiljetpackcompose.navigation.ASBNavigation
import kotlinx.coroutines.delay

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val mainViewModel = MainViewModel()
        setContent {
            MaterialTheme {
                ASBNavigation(mainViewModel = mainViewModel)
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ASBApp(
    mainViewModel: MainViewModel,
    content: @Composable () -> Unit
) {
    val drawerColors = DrawerColors().apply {
        color = colorResource(id = R.color.white)
        background = colorResource(id = R.color.green_500)
        colorLight = colorResource(id = R.color.black)
        backgroundLight = colorResource(id = R.color.gray_200)
        colorSelected = colorResource(id = R.color.white)
        backgroundSelected = colorResource(id = R.color.gray_700)
    }
    NavigationDrawer(
        modifierDrawerContent = Modifier.width(280.dp),
        title = stringResource(id = R.string.top_bar_title),
        drawerState = mainViewModel.drawerState,
        drawerItems = drawerMenuItems,
        headerContent = {
            DrawerHeader()
        },
        navHost = mainViewModel.navHostController,
        colors = drawerColors,
        shape = RectangleShape,
        actionsTopBar = {
            TopBarActions()
        }
    ) {
        content()
    }
}

@Composable
fun TopBarActions() {
    val context = LocalContext.current
    IconButton(onClick = {
        context.toastMessage("Clicked in favorite action.")
    }) {
        Icon(imageVector = Icons.Rounded.Favorite, contentDescription = "Favorite")
    }
}

@Composable
fun DrawerHeader() {
    val animation = remember {
        mutableStateOf(false)
    }
    val scale = remember {
        Animatable(1f)
    }
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colorResource(id = R.color.green_500)),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = stringResource(id = R.string.app_name),
            style = MaterialTheme.typography.displaySmall,
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
                .scale(scale.value)
                .clickable {
                    animation.value = true
                },
            color = Color.White,
            textAlign = TextAlign.Center,
            fontWeight = FontWeight.Bold
        )
    }
    AnimatedVisibility(visible = animation.value) {
        LaunchedEffect(key1 = false) {
            scale.animateTo(
                targetValue = 0.8f,
                animationSpec = tween(
                    durationMillis = 900,
                    easing = {
                        OvershootInterpolator(2f).getInterpolation(it)
                    })
            )
            scale.animateTo(
                targetValue = 1f,
                animationSpec = tween(
                    durationMillis = 900,
                    easing = {
                        OvershootInterpolator(2f).getInterpolation(it)
                    })
            )
            delay(2000L)
            animation.value = false
        }
    }
}

