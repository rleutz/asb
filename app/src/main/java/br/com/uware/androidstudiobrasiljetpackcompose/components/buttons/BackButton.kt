package br.com.uware.androidstudiobrasiljetpackcompose.components.buttons

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.ArrowBack
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController


/**
 * BackButton
 * Back Button.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 01/06/2022 - Initial release.
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun BackButton(
    navController: NavController
) {
    Card(
        modifier = Modifier
            .padding(16.dp)
            .clickable {
                navController.popBackStack()
            },
        elevation = CardDefaults.cardElevation(
            defaultElevation = 16.dp,
            pressedElevation = 0.dp
        ),
        colors = CardDefaults.cardColors(
            containerColor = Color.DarkGray
        )
    ) {
        Row(
            modifier = Modifier.padding(8.dp),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Icon(
                modifier = Modifier.padding(end = 4.dp),
                imageVector = Icons.Rounded.ArrowBack,
                contentDescription = "Back",
                tint = Color.White
            )
            Text(
                text = "Back",
                color = Color.White
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun BackButtonPreview() {
    val navController = rememberNavController()
    BackButton(navController = navController)
}