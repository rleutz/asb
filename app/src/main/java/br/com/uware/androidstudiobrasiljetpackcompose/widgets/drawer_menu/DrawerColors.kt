package br.com.uware.androidstudiobrasiljetpackcompose.widgets.drawer_menu

import androidx.compose.ui.graphics.Color


/**
 * DrawerColors
 * Model for navigation drawer menu.
 * @param color Text color primary.
 * @param background Background color primary.
 * @param colorLight Text light color.
 * @param backgroundLight Background color light.
 * @param colorSelected Color selected item.
 * @param backgroundSelected Background selected item.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 31/05/2022 - Initial release.
 */
data class DrawerColors(
    var color: Color = Color.White,
    var background: Color = Color.DarkGray,
    var colorLight: Color = Color.Black,
    var backgroundLight: Color = Color.LightGray,
    var colorSelected: Color = Color.Black,
    var backgroundSelected: Color = Color.Gray,
)