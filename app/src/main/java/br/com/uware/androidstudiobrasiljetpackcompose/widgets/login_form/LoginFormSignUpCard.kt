package br.com.uware.androidstudiobrasiljetpackcompose.widgets.login_form

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp


/**
 * LoginFormSignUpCard
 * Sign up card showed in login.
 * @param modifier Modifier for card.
 * @param elevation Dp elevation for card.
 * @param colors LoginFormColors.kt model config file.
 * @param titles LoginFormStrings.kt model config file.
 * @param internalPadding Dp for internal padding in card.
 * @param onClick { Unit } for click in card.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 03/06/2022 - Initial release.
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun LoginFormSignUpCard(
    modifier: Modifier = Modifier,
    elevation: Dp = 16.dp,
    colors: LoginFormColors = LoginFormColors(),
    titles: LoginFormStrings = LoginFormStrings(),
    internalPadding: Dp = 8.dp,
    onClick: () -> Unit
) {
    Card(
        modifier = modifier
            .fillMaxWidth()
            .clickable {
                onClick()
            },
        elevation = CardDefaults.cardElevation(
            defaultElevation = elevation,
            pressedElevation = 0.dp
        )
    ) {
        Column(
            modifier = Modifier.padding(internalPadding)
        ) {
            Text(
                modifier = Modifier.fillMaxWidth(),
                text = titles.newUser,
                fontWeight = FontWeight.Bold,
                color = colors.disabledButton,
                style = MaterialTheme.typography.titleMedium,
                textAlign = TextAlign.Center
            )
            LoginFormTextWithShadow(
                modifier = Modifier.fillMaxWidth(),
                text = titles.signUp,
                fontWeight = FontWeight.Bold,
                style = MaterialTheme.typography.titleLarge,
                color = colors.color
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun LoginFormSignUpCardPreview() {
    LoginFormSignUpCard(
        onClick = {}
    )
}