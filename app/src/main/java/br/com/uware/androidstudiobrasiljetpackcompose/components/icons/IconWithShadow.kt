package br.com.uware.androidstudiobrasiljetpackcompose.components.icons

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Android
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp


/**
 * IconWithShadow
 * ImageVector with shadow.
 * @param icon ImageVector for icon.
 * @param description String description for icon.
 * @param modifier Modifier
 * @param color Color for icon.
 * @param shadowColor Color for shadow.
 * @param offset Dp of shadow offset.
 * @param alpha Float of shadow opacity.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 01/06/2022 - Initial release.
 */
@Composable
fun IconWithShadow(
    icon: ImageVector,
    description: String,
    modifier: Modifier = Modifier,
    color: Color = Color.White,
    shadowColor: Color = Color.DarkGray,
    offset: Dp = 2.dp,
    alpha: Float = 0.65f
) {
    Box {
        Icon(
            modifier = modifier
                .offset(
                    x = offset,
                    y = offset
                )
                .alpha(alpha),
            imageVector = icon,
            contentDescription = description,
            tint = shadowColor
        )
        Icon(
            modifier = modifier,
            imageVector = icon,
            contentDescription = description,
            tint = color
        )
    }
}


@Preview(showBackground = true)
@Composable
fun IconWithShadowPreview(){
    IconWithShadow(
        modifier = Modifier.size(150.dp).padding(25.dp),
        color = Color.Blue,
        icon = Icons.Rounded.Android, description = "Android"
    )
}