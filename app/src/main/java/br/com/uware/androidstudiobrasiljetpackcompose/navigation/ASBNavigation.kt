package br.com.uware.androidstudiobrasiljetpackcompose.navigation

import androidx.compose.material3.DrawerValue
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import br.com.uware.androidstudiobrasiljetpackcompose.ASBApp
import br.com.uware.androidstudiobrasiljetpackcompose.MainViewModel
import br.com.uware.androidstudiobrasiljetpackcompose.screens.testscreen.TestScreen
import br.com.uware.androidstudiobrasiljetpackcompose.screens.animation.AnimationScreen
import br.com.uware.androidstudiobrasiljetpackcompose.screens.choice_row.ChoiceRowScreen
import br.com.uware.androidstudiobrasiljetpackcompose.screens.drag_drop_list.DragDropListScreen
import br.com.uware.androidstudiobrasiljetpackcompose.screens.home.HomeScreen
import br.com.uware.androidstudiobrasiljetpackcompose.screens.input_password_field.InputPasswordFieldScreen
import br.com.uware.androidstudiobrasiljetpackcompose.screens.login.LoginScreen
import br.com.uware.androidstudiobrasiljetpackcompose.screens.round_button.RoundButtonScreen
import br.com.uware.androidstudiobrasiljetpackcompose.screens.splash.SplashScreen
import br.com.uware.androidstudiobrasiljetpackcompose.screens.text_with_shadow.TextWithShadowScreen
import br.com.uware.androidstudiobrasiljetpackcompose.screens.toast.ToastScreen


/**
 * ASBNavigation
 * NavHost of app.
 * @param mainViewModel MainViewModel of app.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 30/05/2022 - Initial release.
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ASBNavigation(mainViewModel: MainViewModel) {
    mainViewModel.navHostController = rememberNavController()
    mainViewModel.drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)
    mainViewModel.coroutineScope = rememberCoroutineScope()
    NavHost(
        navController = mainViewModel.navHostController,
        startDestination = ASBScreens.SplashScreen.name
    ) {

        composable(ASBScreens.SplashScreen.name){
            SplashScreen(mainViewModel = mainViewModel)
        }

        composable(ASBScreens.HomeScreen.name){
            ASBApp(mainViewModel = mainViewModel) {
                HomeScreen(mainViewModel = mainViewModel)
            }
        }

        composable(ASBScreens.ToastScreen.name){
            ASBApp(mainViewModel = mainViewModel) {
                ToastScreen()
            }
        }

        composable(ASBScreens.InputTextFieldScreen.name){

        }

        composable(ASBScreens.InputPasswordFieldScreen.name){
            ASBApp(mainViewModel = mainViewModel) {
                InputPasswordFieldScreen(mainViewModel)
            }
        }

        composable(ASBScreens.ChoiceRowScreen.name){
            ASBApp(mainViewModel = mainViewModel) {
                ChoiceRowScreen()
            }
        }

        composable(ASBScreens.AnimationScreen.name){
            ASBApp(mainViewModel = mainViewModel) {
                AnimationScreen()
            }
        }

        composable(ASBScreens.RoundButtonScreen.name){
            ASBApp(mainViewModel = mainViewModel) {
                RoundButtonScreen(mainViewModel)
            }
        }

        composable(ASBScreens.DragDropListScreen.name){
            ASBApp(mainViewModel = mainViewModel) {
                DragDropListScreen(mainViewModel = mainViewModel)
            }
        }

        composable(ASBScreens.TextWithShadowScreen.name){
            ASBApp(mainViewModel = mainViewModel) {
                TextWithShadowScreen(mainViewModel = mainViewModel)
            }
        }

        composable(ASBScreens.LoginScreen.name){
            ASBApp(mainViewModel = mainViewModel) {
                LoginScreen(mainViewModel)
            }
        }
    }
}