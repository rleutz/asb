package br.com.uware.androidstudiobrasiljetpackcompose.widgets.login_form

import androidx.compose.foundation.layout.*
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp


/**
 * LoginFormTitle
 *
 * @author Rodrigo Leutz
 * @version 1.0.0 - 03/06/2022 - Initial release.
 */
@Composable
fun LoginFormTitle(
    isLogin: Boolean,
    isForgot: Boolean,
    colors: LoginFormColors = LoginFormColors(),
    titles: LoginFormStrings = LoginFormStrings(),
    icons: LoginFormIcons = LoginFormIcons()
) {
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically
    ) {
        LoginFormIconWithShadow(
            modifier = Modifier
                .size(50.dp)
                .padding(end = 8.dp),
            icon = if (isLogin && isForgot) icons.forgotPassword
            else if (isLogin) icons.login else icons.register,
            description = if (isLogin) titles.login else titles.register,
            color = colors.color
        )
        Text(
            text = if (isLogin && isForgot) titles.forgotPassword
            else if (isLogin) titles.login else titles.register,
            fontWeight = FontWeight.Bold,
            style = MaterialTheme.typography.titleMedium,
            color = colors.color
        )
    }
}

@Preview(showBackground = true)
@Composable
fun LoginFormTitlePreview() {
    LoginFormTitle(isLogin = true, isForgot = false)
}