package br.com.uware.androidstudiobrasiljetpackcompose.functions

import android.content.Context
import android.widget.Toast


/**
 * ToastMessage
 * Send a Toast message for user.
 * @param message String of message.
 * @param duration Int of Toast duration.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 30/05/2022 - Initial release.
 */
fun Context.toastMessage(
    message: String,
    duration: Int = Toast.LENGTH_SHORT
) {
    Toast.makeText(this, message, duration).show()
}