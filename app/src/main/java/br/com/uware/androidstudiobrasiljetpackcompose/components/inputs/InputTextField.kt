package br.com.uware.androidstudiobrasiljetpackcompose.components.inputs

import androidx.compose.foundation.background
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp


/**
 * InputTextField
 * EditText for Jetpack Compose.
 * @param modifier Modifier
 * @param valueState MutableState<String> for value.
 * @param title String for title in InputField.
 * @param color Int of resource color.
 * @param leadingIcon { Unit } for leading icon = { }.
 * @param trailingIcon { Unit } for trailing icon = { }.
 * @param enable Boolean = true
 * @param isSingleLine Boolean for single line = true.
 * @param keyboardType KeyboardType = Text.
 * @param imeAction = ImeAction for keyboard = Next.
 * @param onAction KeyboardAction = Default.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 2022 04 16 - Initial release.
 * @version 1.0.1 - 30/05/2022 - Remove default color - Rodrigo Leutz
 * @version 1.0.2 - 30/05/2022 - Change to material3 - Rodrigo Leutz
 * @version 1.0.3 - 30/05/2022 - Added @Preview and changed color dependency - Wagner Arcieri
 */
@Composable
fun InputTextField(
    modifier: Modifier = Modifier,
    valueState: MutableState<String>,
    title: String,
    color: Color,
    leadingIcon: @Composable () -> Unit = {},
    trailingIcon: @Composable () -> Unit = {},
    enable: Boolean = true,
    isSingleLine: Boolean = true,
    keyboardType: KeyboardType = KeyboardType.Text,
    imeAction: ImeAction = ImeAction.Next,
    onAction: KeyboardActions = KeyboardActions.Default,
) {
    OutlinedTextField(
        value = valueState.value, onValueChange = { valueState.value = it },
        label = { Text(text = title) },
        singleLine = isSingleLine,
        textStyle = TextStyle(fontSize = 18.sp, color = MaterialTheme.colorScheme.onBackground),
        enabled = enable,
        keyboardOptions = KeyboardOptions(keyboardType = keyboardType, imeAction = imeAction),
        keyboardActions = onAction,
        leadingIcon = { leadingIcon() },
        trailingIcon = { trailingIcon() },
        modifier = modifier,
        colors = TextFieldDefaults.outlinedTextFieldColors(
            focusedBorderColor = color,
            unfocusedBorderColor = color,
            focusedLabelColor = color,
            cursorColor = color
        )
    )
}

@Preview
@Composable
fun InputTextFieldPreview() {

    val inputTitle = "password"
    val valueState = remember { mutableStateOf("") }
    val color  = Color(0xFFFFFFFF)

    Surface(
        modifier = Modifier
            .background(Color(0xFFFFFFFF))
    ) {
        InputTextField(valueState = valueState, title = inputTitle, color = color)
    }

}