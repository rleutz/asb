package br.com.uware.androidstudiobrasiljetpackcompose.components.inputs


import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Visibility
import androidx.compose.material.icons.rounded.VisibilityOff
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp


/**
 * InputTextField
 * @param modifier Modifier
 * @param passwordState MutableState<String> for password.
 * @param title String for title.
 * @param passwordVisibility MutableState<Boolean> for password visibility.
 * @param enabled Boolean = true
 * @param color Color for input password.
 * @param trailingIconColor Color for trailing icon.
 * @param imeAction = ImeAction for keyboard = Next.
 * @param onAction KeyboardAction = Default.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 13/05/2022 - Initial release.
 */
@Composable
fun InputPasswordField(
    passwordState: MutableState<String>,
    title: String,
    passwordVisibility: MutableState<Boolean>,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    color: Color = Color.Black,
    trailingIconColor: Color = Color.DarkGray,
    imeAction: ImeAction = ImeAction.Done,
    onAction: KeyboardActions = KeyboardActions.Default,
) {
    val visualTransformation = if (passwordVisibility.value) VisualTransformation.None else
        PasswordVisualTransformation()
    OutlinedTextField(
        value = passwordState.value,
        onValueChange = {
            passwordState.value = it
        },
        label = { Text(text = title) },
        singleLine = true,
        textStyle = TextStyle(fontSize = 18.sp, color = MaterialTheme.colorScheme.onBackground),
        modifier = modifier
            .fillMaxWidth(),
        enabled = enabled,
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Password,
            imeAction = imeAction
        ),
        visualTransformation = visualTransformation,
        trailingIcon = { PasswordVisibility(passwordVisibility = passwordVisibility) },
        keyboardActions = onAction,
        colors = TextFieldDefaults.outlinedTextFieldColors(
            focusedBorderColor = color,
            unfocusedBorderColor = color,
            focusedLabelColor = color,
            cursorColor = color,
            focusedTrailingIconColor = trailingIconColor,
            disabledTrailingIconColor = trailingIconColor
        )
    )
}

/**
 * PasswordVisibility
 * Change the icon of input password.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 13/05/2022 - Initial release.
 */
@Composable
fun PasswordVisibility(passwordVisibility: MutableState<Boolean>) {
    val visible = passwordVisibility.value
    IconButton(
        onClick = {
            passwordVisibility.value = !visible
        }
    ) {
        Icon(
            imageVector = if(visible) Icons.Rounded.VisibilityOff
            else Icons.Rounded.Visibility,
            contentDescription = "Password Visibility"
        )
    }
}

@Preview
@Composable
fun InputPasswordFieldPreview() {

    val passTitle = "password"
    val passState = remember { mutableStateOf("") }
    val passVisibility = remember { mutableStateOf(true) }

    Surface(
        modifier = Modifier
            .background(Color(0xFFFFFFFF))
    ) {
        InputPasswordField(passwordState = passState, title = passTitle, passwordVisibility = passVisibility)
    }

}