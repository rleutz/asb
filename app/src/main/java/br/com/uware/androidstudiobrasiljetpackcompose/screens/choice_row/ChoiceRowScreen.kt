package br.com.uware.androidstudiobrasiljetpackcompose.screens.choice_row

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import br.com.uware.androidstudiobrasiljetpackcompose.functions.toastMessage
import br.com.uware.androidstudiobrasiljetpackcompose.widgets.ChoiceRow


/**
 * ChoiceRowScreen
 *
 * @author Rodrigo Leutz
 * @version 1.0.0 - 31/05/2022 - Initial release.
 */
@Composable
fun ChoiceRowScreen(){
    val context = LocalContext.current
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        ChoiceRow(
            acceptTitle = "Accept",
            denyTitle = "Deny",
            acceptOnClick = {
                context.toastMessage("Accept button clicked")
            },
            denyOnClick = {
                context.toastMessage("Deny button clicked")
            }
        )
    }
}

@Preview(showBackground = true)
@Composable
fun ChoiceRowScreenPreview(){
    ChoiceRowScreen()
}