package br.com.uware.androidstudiobrasiljetpackcompose.widgets.drawer_menu


import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Menu
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import br.com.uware.androidstudiobrasiljetpackcompose.components.layouts.DrawerScaffold
import kotlinx.coroutines.launch


/**
 * NavigationDrawer
 * Navigation Drawer menu.
 * @param modifier Modifier for content.
 * @param title String title in top bar.
 * @param modifierDrawerContent Modifier for DrawerContent
 * @param headModifier Modifier for drawer head content.
 * @param roundCorner Dp for corners in drawer menu.
 * @param drawerState DrawerState open or closed.
 * @param drawerItems ArrayList<DrawerMenuItem> Items in drawer menu.
 * @param drawerShape Shape for drawer menu.
 * @param drawerTonalElevation Dp of tonal elevation.
 * @param gestureEnabled Boolean gesture is enabled to open drawer.
 * @param navIcon ImageVector of icon in nav.
 * @param actionsTopBar { @Unit } IconButtons in actions of top bar.
 * @param navHost NavHostController.
 * @param colors DrawerColors model in package.
 * @param shape Shape for drawer item.
 * @param headerContent { @Unit } Header content of drawer menu.
 * @param content { @Unit } Content in screen.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 30/05/2022 - Initial release.
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun NavigationDrawer(
    modifier: Modifier = Modifier,
    title: String = "",
    modifierDrawerContent: Modifier = Modifier.fillMaxSize(),
    headModifier: Modifier = Modifier,
    roundCorner: Dp = 16.dp,
    drawerState: DrawerState = rememberDrawerState(initialValue = DrawerValue.Closed),
    drawerItems: ArrayList<DrawerMenuItem> = ArrayList(),
    drawerShape: Shape = RectangleShape,
    drawerTonalElevation: Dp = 16.dp,
    gestureEnabled: Boolean = true,
    navIcon: ImageVector = Icons.Rounded.Menu,
    actionsTopBar: @Composable () -> Unit = {},
    navHost: NavHostController,
    colors: DrawerColors = DrawerColors(),
    shape: Shape = RoundedCornerShape(topEnd = roundCorner, bottomEnd = roundCorner),
    headerContent: @Composable () -> Unit = {},
    content: @Composable () -> Unit = {}
) {
    val coroutineScope = rememberCoroutineScope()
    ModalNavigationDrawer(
        modifier = modifier,
        drawerState = drawerState,
        gesturesEnabled = gestureEnabled,
        drawerTonalElevation = drawerTonalElevation,
        drawerShape = drawerShape,
        drawerContent = {
            DrawerContent(
                modifier = modifierDrawerContent,
                colors = colors,
                navHost = navHost,
                drawerItems = drawerItems,
                drawerState = drawerState,
                headModifier = headModifier,
                headerContent = { headerContent() },
                roundCorner = roundCorner,
                shape = shape
            )
        },
        drawerContainerColor = Color.Transparent
    ) {
        DrawerScaffold(
            topBar = {
                DrawerTopBar(
                    title = title,
                    colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
                        containerColor = colors.background,
                        scrolledContainerColor = colors.background,
                        titleContentColor = colors.color,
                        navigationIconContentColor = colors.color,
                        actionIconContentColor = colors.color
                    ),
                    navDescription = title,
                    navIcon = navIcon,
                    navOnClick = {
                        coroutineScope.launch {
                            drawerState.open()
                        }
                    },
                    actions = { actionsTopBar() }
                )
            }
        ) {
            content()
        }
    }
}

/**
 * DrawerContent
 * Content of drawer menu.
 * @param modifier Modifier.
 * @param headerContent { @Unit } Header content of drawer menu.
 * @param headModifier Modifier for drawer head content.
 * @param navHost NavHostController.
 * @param colors DrawerColors model in package.
 * @param roundCorner Dp for corners in drawer menu.
 * @param drawerState DrawerState open or closed.
 * @param shape Shape for drawer item.
 * @param drawerItems ArrayList<DrawerMenuItem> Buttons of drawer menu.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 31/05/2022 - Initial release.
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DrawerContent(
    modifier: Modifier = Modifier,
    headerContent: @Composable () -> Unit = {},
    headModifier: Modifier = Modifier,
    navHost: NavHostController,
    colors: DrawerColors = DrawerColors(),
    roundCorner: Dp = 16.dp,
    drawerState: DrawerState,
    shape: Shape = RoundedCornerShape(topEnd = roundCorner, bottomEnd = roundCorner),
    drawerItems: ArrayList<DrawerMenuItem> = ArrayList()
) {
    Column(modifier = modifier.fillMaxHeight()) {
        Box(
            modifier = headModifier
                .fillMaxWidth()
                .height(200.dp)
                .background(colors.background),
            contentAlignment = Alignment.Center
        ) {
            headerContent()
        }
        LazyColumn(modifier = Modifier
            .fillMaxHeight()
            .background(colors.backgroundLight)) {
            items(drawerItems) { item ->
                DrawerItem(
                    navHost = navHost,
                    navigate = item.destination,
                    title = item.title,
                    colors = NavigationDrawerItemDefaults.colors(
                        selectedTextColor = colors.colorSelected,
                        unselectedTextColor = colors.colorLight,
                        selectedIconColor = colors.colorSelected,
                        unselectedIconColor = colors.colorLight,
                        selectedContainerColor = colors.backgroundSelected,
                        unselectedContainerColor = colors.backgroundLight,
                        selectedBadgeColor = colors.colorSelected,
                        unselectedBadgeColor = colors.colorLight
                    ),
                    drawerState = drawerState,
                    roundCorner = roundCorner,
                    shape = shape,
                    icon = item.icon
                )
            }
        }
    }
}

/**
 * DrawerItem
 * Drawer item in menu.
 * @param modifier Modifier.
 * @param icon ImageVector of icon.
 * @param badge { Unit } Badge.
 * @param navHost NavHostController.
 * @param drawerState DrawerState open or closed.
 * @param navigate String of destination.
 * @param title String of title.
 * @param roundCorner Dp for corners in drawer menu.
 * @param colors NavigationDrawerItemColors.
 * @param shape Shape for drawer item.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 31/05/2022 - Initial release.
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DrawerItem(
    modifier: Modifier = Modifier,
    icon: ImageVector,
    badge: () -> Unit = {},
    navHost: NavHostController,
    drawerState: DrawerState,
    navigate: String,
    title: String,
    roundCorner: Dp = 16.dp,
    colors: NavigationDrawerItemColors = NavigationDrawerItemDefaults.colors(),
    shape: Shape = RoundedCornerShape(topEnd = roundCorner, bottomEnd = roundCorner)
) {
    val coroutineScope = rememberCoroutineScope()
    NavigationDrawerItem(
        modifier = modifier,
        label = {
            Text(
                text = title,
                fontWeight = FontWeight.Bold
            )
        },
        selected = navHost.currentDestination?.route == navigate,
        onClick = {
            coroutineScope.launch {
                drawerState.close()
                navHost.navigate(navigate)
            }
        },
        icon = {
            Icon(imageVector = icon, contentDescription = title)
        },
        badge = { badge() },
        shape = shape,
        colors = colors
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Preview
@Composable
fun NavigationDrawerPreview() {
    val navController = rememberNavController()
    val drawerState = rememberDrawerState(initialValue = DrawerValue.Open)
    NavigationDrawer(
        drawerState = drawerState,
        navHost = navController
    ) {

    }
}