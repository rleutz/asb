package br.com.uware.androidstudiobrasiljetpackcompose.widgets.drawer_menu

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Menu
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector


/**
 * CenterTopBar
 * Center top align bar.
 * @param modifier Modifier.
 * @param title String of title.
 * @param colors TopAppBarColors.
 * @param navIcon ImageVector for icon.
 * @param navDescription String for nav description.
 * @param actions { Unit } Icons and functions for action, end of bar.
 * @param navOnClick { Unit } functions for click in nav.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 30/05/2022 - Initial release.
 * @version 1.0.1 - 30/05/2022 - Added @Preview fun - Wagner Arcieri
 */
@Composable
fun DrawerTopBar(
    modifier: Modifier = Modifier,
    title: String,
    colors: TopAppBarColors = TopAppBarDefaults.centerAlignedTopAppBarColors(),
    navIcon: ImageVector = Icons.Rounded.Menu,
    navDescription: String = title,
    actions: @Composable () -> Unit = {},
    navOnClick: () -> Unit = {}
) {
    CenterAlignedTopAppBar(
        title = {
            Text(text = title)
        },
        modifier = modifier,
        navigationIcon = {
            IconButton(onClick = {
                navOnClick()
            }) {
                Icon(imageVector = navIcon, contentDescription = navDescription)
            }
        },
        actions = {
            actions()
        },
        colors = colors
    )
}