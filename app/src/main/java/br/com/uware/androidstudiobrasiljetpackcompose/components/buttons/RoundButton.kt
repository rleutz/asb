package br.com.uware.androidstudiobrasiljetpackcompose.components.buttons

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import br.com.uware.androidstudiobrasiljetpackcompose.R


/**
 * RoundButton
 *
 * Customizable Round Button with Border based on a Card and an Icon
 *
 * This Button use an Int ID for the Icon,
 * please consider creating a new vector image in resources drawables for it.
 *
 * @param padding The Unit in Dp of padding value for the Button Modifier. Default = 1.dp
 * @param elevation The Unit in Dp of elevation value for the Button Modifier. Default = 2.dp
 * @param buttonSize The Unit in Dp for the Button size. Default = 30.dp
 * @param backgroundColor The background Color of the Button. Default = Color(0x1A000000)(Black at 10% alpha)
 * @param borderStroke The Unit in Dp of border value for the Button Modifier. Default = 1.dp
 * @param borderColor Button border Color. Default = Color(0xFF0000FF)
 * @param iconID Int with the ID of the the Icon in Resources, Default = R.drawable.ic_launcher_foreground
 * @param iconSize The Unit in Dp for the Size of the Icon inside the Button. Default = 20.dp
 * @param iconDescription A String with the Icon/Button description. Default = ""
 * @param iconColor Color of the Icon inside of the Button. Default = Color(0xFF0000FF)
 * @param onClick The Method invoked on the Button Click listener. Default = { }
 *
 * @author Wagner Arcieri
 * @version 1.0.0 - 30/05/2022 - Initial Release
 * @version 1.0.1 - 30/05/2022 - Change resource import - Rodrigo Leutz
 * @version 1.0.2 - 30/05/2022 - Change to material3 - Rodrigo Leutz
 * @version 1.0.3 - 30/05/2022 - params desc text change - Wagner Arcieri
 *
 * */


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun RoundButton(
    padding: Dp = 1.dp,
    elevation: Dp = 2.dp,
    buttonSize: Dp = 30.dp,
    backgroundColor: Color = Color(0x1A000000),
    borderStroke: Dp = 1.dp,
    borderColor: Color = Color(0xFF0000FF),
    iconID: Int = R.drawable.ic_launcher_foreground,
    iconSize: Dp = 20.dp,
    iconDescription: String = "",
    iconColor: Color = Color(0xFF0000FF),
    onClick: () -> Unit = { }
    ) {
    Card(
        modifier = Modifier
            .padding(padding)
            .size(buttonSize)
            .clickable {
                onClick.invoke()
            },
        colors = CardDefaults.cardColors(
            containerColor = backgroundColor
        ),
        shape = RoundedCornerShape(buttonSize),
        elevation = CardDefaults.cardElevation(
            defaultElevation = elevation
        ),
        border = BorderStroke(
            width = borderStroke,
            color = borderColor
        )
    ) {
        Row(
            Modifier.fillMaxSize(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            Icon(
                modifier = Modifier
                    .size(iconSize),
                painter = painterResource(id = iconID),
                contentDescription = iconDescription,
                tint = iconColor


            )
        }
    }
}


/**
 *  Button is nested in a Column for centralization
 *  and is over a colored Surface for better Preview
*/
@Preview(showBackground = true)
@Composable
fun RoundButtonPreview() {


    
    Surface(modifier = Modifier.size(50.dp), color = Color(0xFFFFFFFF)) {

        Column(
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            RoundButton()
        }
        

        
    }
    
}
