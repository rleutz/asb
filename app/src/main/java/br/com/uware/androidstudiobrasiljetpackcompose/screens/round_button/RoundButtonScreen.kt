package br.com.uware.androidstudiobrasiljetpackcompose.screens.round_button

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import br.com.uware.androidstudiobrasiljetpackcompose.MainViewModel
import br.com.uware.androidstudiobrasiljetpackcompose.R
import br.com.uware.androidstudiobrasiljetpackcompose.components.buttons.RoundButton


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun RoundButtonScreen(mainViewModel: MainViewModel) {

    Surface(
        modifier = Modifier
            .fillMaxSize()
    ) {
        Row() {
            Image(
                modifier = Modifier.fillMaxSize(),
                contentScale = ContentScale.Crop,
                painter = painterResource(id = R.drawable.sample_background),
                contentDescription = "background image"
            )
        }

        Column(
            modifier = Modifier
                .fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceEvenly
        ) {

            Row(
                modifier = Modifier
                    .fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceAround
            ) {
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    RoundButton(
                        buttonSize = 100.dp,
                        onClick = { },
                        backgroundColor = Color(0xFF0000FF),
                        borderStroke = 4.dp,
                        borderColor = Color(0xFFFFFFFF),
                        elevation = 0.dp,
                        iconID = R.drawable.ic_edit_pencil,
                        iconSize = 60.dp,
                        iconDescription = "EDIT Recipe Button",
                        iconColor = Color(0xFFFFFFFF)
                    )
                    Card(

                        colors = CardDefaults.cardColors(Color(0xFFE2E2E2)),
                        border = BorderStroke(1.dp, Color(0xFFc0c0c0))
                    ) {
                        Text(
                            modifier = Modifier
                                .padding(2.dp),
                            text = "elevation = 0.dp"
                        )
                        Text(
                            modifier = Modifier
                                .padding(2.dp),
                            text = "border = 4.dp"
                        )
                    }

                }
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    RoundButton(
                        buttonSize = 100.dp,
                        onClick = { },
                        backgroundColor = Color(0xFF0000FF),
                        borderStroke = 4.dp,
                        borderColor = Color(0xFFFFFFFF),
                        elevation = 6.dp,
                        iconID = R.drawable.ic_edit_pencil,
                        iconSize = 60.dp,
                        iconDescription = "EDIT Recipe Button",
                        iconColor = Color(0xFFFFFFFF)
                    )
                    Card(
                        colors = CardDefaults.cardColors(Color(0xFFE2E2E2)),
                        border = BorderStroke(1.dp, Color(0xFFc0c0c0))
                    ) {
                        Text(
                            modifier = Modifier
                                .padding(2.dp),
                            text = "elevation = 6.dp"
                        )
                        Text(
                            modifier = Modifier
                                .padding(2.dp),
                            text = "border = 3.dp"
                        )
                    }
                }

            }

            Row(
                modifier = Modifier
                    .fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceAround
            ) {
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    RoundButton(
                        buttonSize = 100.dp,
                        onClick = { },
                        backgroundColor = Color(0xFF0000FF),
                        borderStroke = 0.dp,
                        borderColor = Color(0x00FFFFFF),
                        elevation = 0.dp,
                        iconID = R.drawable.ic_edit_pencil,
                        iconSize = 60.dp,
                        iconDescription = "EDIT Recipe Button",
                        iconColor = Color(0xFFFFFFFF)
                    )
                    Card(

                        colors = CardDefaults.cardColors(Color(0xFFE2E2E2)),
                        border = BorderStroke(1.dp, Color(0xFFc0c0c0))
                    ) {
                        Text(
                            modifier = Modifier
                                .padding(2.dp),
                            text = "elevation = 0.dp"
                        )
                        Text(
                            modifier = Modifier
                                .padding(2.dp),
                            text = "border = 0.dp"
                        )
                    }

                }
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    RoundButton(
                        buttonSize = 100.dp,
                        onClick = { },
                        backgroundColor = Color(0xFF0000FF),
                        borderStroke = 0.dp,
                        borderColor = Color(0x00FFFFFF),
                        elevation = 6.dp,
                        iconID = R.drawable.ic_edit_pencil,
                        iconSize = 60.dp,
                        iconDescription = "EDIT Recipe Button",
                        iconColor = Color(0xFFFFFFFF)
                    )
                    Card(
                        colors = CardDefaults.cardColors(Color(0xFFE2E2E2)),
                        border = BorderStroke(1.dp, Color(0xFFc0c0c0))
                    ) {
                        Text(
                            modifier = Modifier
                                .padding(2.dp),
                            text = "elevation = 6.dp"
                        )
                        Text(
                            modifier = Modifier
                                .padding(2.dp),
                            text = "border = 0.dp"
                        )
                    }
                }

            }

            Row(
                modifier = Modifier
                    .fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceAround
            ) {
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    RoundButton(
                        buttonSize = 100.dp,
                        onClick = { },
                        backgroundColor = Color(0x99FFFFFF),
                        borderStroke = 4.dp,
                        borderColor = Color(0xFF0000FF),
                        elevation = 0.dp,
                        iconID = R.drawable.ic_edit_pencil,
                        iconSize = 60.dp,
                        iconDescription = "EDIT Recipe Button",
                        iconColor = Color(0xFF0000FF)
                    )
                    Card(

                        colors = CardDefaults.cardColors(Color(0xFFE2E2E2)),
                        border = BorderStroke(1.dp, Color(0xFFc0c0c0))
                    ) {
                        Text(
                            modifier = Modifier
                                .padding(2.dp),
                            text = "elevation = 0.dp"
                        )
                        Text(
                            modifier = Modifier
                                .padding(2.dp),
                            text = "border = 4.dp"
                        )
                    }

                }
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    RoundButton(
                        buttonSize = 100.dp,
                        onClick = { },
                        backgroundColor = Color(0x99FFFFFF),
                        borderStroke = 3.dp,
                        borderColor = Color(0xFF0000FF),
                        elevation = 1.dp,
                        iconID = R.drawable.ic_edit_pencil,
                        iconSize = 60.dp,
                        iconDescription = "EDIT Recipe Button",
                        iconColor = Color(0xFF0000FF)
                    )
                    Card(
                        colors = CardDefaults.cardColors(Color(0xFFE2E2E2)),
                        border = BorderStroke(1.dp, Color(0xFFc0c0c0))
                    ) {
                        Text(
                            modifier = Modifier
                                .padding(2.dp),
                            text = "elevation = 1.dp"
                        )
                        Text(
                            modifier = Modifier
                                .padding(2.dp),
                            text = "border = 3.dp"
                        )
                    }
                }

            }

            Row(
                modifier = Modifier
                    .fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceAround
            ) {
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    RoundButton(
                        buttonSize = 100.dp,
                        onClick = { },
                        backgroundColor = Color(0x00FFFFFF),
                        borderStroke = 4.dp,
                        borderColor = Color(0xFF0000FF),
                        elevation = 0.dp,
                        iconID = R.drawable.ic_edit_pencil,
                        iconSize = 60.dp,
                        iconDescription = "EDIT Recipe Button",
                        iconColor = Color(0xFF0000FF)
                    )
                    Card(

                        colors = CardDefaults.cardColors(Color(0xFFE2E2E2)),
                        border = BorderStroke(1.dp, Color(0xFFc0c0c0))
                    ) {
                        Text(
                            modifier = Modifier
                                .padding(2.dp),
                            text = "elevation = 0.dp"
                        )
                        Text(
                            modifier = Modifier
                                .padding(2.dp),
                            text = "border = 4.dp"
                        )
                        Text(
                            modifier = Modifier
                                .padding(2.dp),
                            text = "TRANSPARENT"
                        )
                    }

                }
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    RoundButton(
                        buttonSize = 100.dp,
                        onClick = { },
                        backgroundColor = Color(0x00FFFFFF),
                        borderStroke = 3.dp,
                        borderColor = Color(0xFF0000FF),
                        elevation = 1.dp,
                        iconID = R.drawable.ic_edit_pencil,
                        iconSize = 60.dp,
                        iconDescription = "EDIT Recipe Button",
                        iconColor = Color(0xFF0000FF)
                    )
                    Card(
                        colors = CardDefaults.cardColors(Color(0xFFE2E2E2)),
                        border = BorderStroke(1.dp, Color(0xFFc0c0c0))
                    ) {
                        Text(
                            modifier = Modifier
                                .padding(2.dp),
                            text = "elevation = 1.dp"
                        )
                        Text(
                            modifier = Modifier
                                .padding(2.dp),
                            text = "border = 3.dp"
                        )
                        Text(
                            modifier = Modifier
                                .padding(2.dp),
                            text = "TRANSPARENT"
                        )
                    }
                }

            }

            OutlinedButton(
                onClick = {
                    mainViewModel.navHostController.popBackStack()
                },
                modifier = Modifier.padding(16.dp),
                elevation = ButtonDefaults.buttonElevation(
                    defaultElevation = 8.dp,
                    pressedElevation = 0.dp
                )
            ) {
                Text(text = "Back")
            }

        }
    }
}

@Preview(showBackground = true)
@Composable
fun RoundButtonScreenPreview(){

    val mainViewModel = MainViewModel()

    RoundButtonScreen(mainViewModel)
}