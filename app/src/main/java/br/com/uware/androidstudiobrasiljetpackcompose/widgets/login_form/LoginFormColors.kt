package br.com.uware.androidstudiobrasiljetpackcompose.widgets.login_form

import androidx.compose.ui.graphics.Color


/**
 * LoginFormColors
 * Login form colors model or config file to use.
 * @param color Color default for objects in login form.
 * @param background Color background for login form.
 * @param acceptButton Color for accept button in choice row.
 * @param denyButton Color for deny button in choice row.
 * @param acceptButtonText Color for text in accept button of the choice row.
 * @param denyButtonText Color for text in deny button of the choice row.
 * @param disabledButton Color for disabled button in choice row.
 * @param disabledButtonText Color for text in disabled button of the choice row.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 02/06/2022 - Initial release.
 */
data class LoginFormColors(
    var color: Color = Color(0xFF04104D),
    var background: Color = Color(0xFFd3d3d3),
    var acceptButton: Color = Color(0xFF2E5A22),
    var denyButton: Color = Color(0xFF7C1313),
    var acceptButtonText: Color = Color(0xFFFFFFFF),
    var denyButtonText: Color = Color(0xFFFFFFFF),
    var disabledButton: Color = Color(0xFF3C3A3A),
    var disabledButtonText: Color = Color(0xFFFFFFFF)
)