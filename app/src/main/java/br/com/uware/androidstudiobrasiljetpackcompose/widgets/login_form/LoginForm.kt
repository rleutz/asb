package br.com.uware.androidstudiobrasiljetpackcompose.widgets.login_form

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Android
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.launch


/**
 * LoginForm
 * Login and register form.
 * @param modifier Modifier for Column.
 * @param headerContent { @Unit } Header logo content.
 * @param colors LoginFormColors.kt data class file.
 * @param titles LoginFormStrings.kt data class file.
 * @param signUpCardModifier Modifier for sign up card.
 * @param signUpCardInternalPadding Dp for internal padding in sign up card.
 * @param signUpCardElevation Dp for sign up card elevation.
 * @param emailValidate { Unit }: Boolean logic to validate email.
 * @param passwordValidate { Unit }: Boolean logic to validate password.
 * @param policyPrivacyOnClick { @Unit } execute when click policy privacy in register screen.
 * @param termsOfUseOnClick { @Unit } execute when click terms of use in register screen.
 * @param loginAccept { email, password -> }: String? receive when click accept in login, return string in snackBar or null.
 * @param loginDeny { Unit } executes when click deny in login.
 * @param registerAccept { name, email, password -> }: String? receive when click accept in register, return string in snackBar or null.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 02/06/2022 - Initial release.
 */
@Composable
fun LoginForm(
    modifier: Modifier = Modifier,
    headerContent: @Composable () -> Unit = {},
    colors: LoginFormColors = LoginFormColors(),
    titles: LoginFormStrings = LoginFormStrings(),
    signUpCardModifier: Modifier = Modifier,
    signUpCardInternalPadding: Dp = 8.dp,
    signUpCardElevation: Dp = 16.dp,
    emailValidate: (email: String) -> Boolean = { email ->
        email.trim().isNotEmpty() &&
                email.contains("@") &&
                email.split("@")[1].length > 3 &&
                email.split("@")[1].contains(".")
    },
    passwordValidate: (password: String) -> Boolean = { password ->
        password.trim().isNotEmpty()
    },
    policyPrivacyOnClick: () -> Unit = {},
    termsOfUseOnClick: () -> Unit = {},
    loginAccept: (email: String, password: String) -> String?,
    loginDeny: () -> Unit,
    forgotPasswordOnClick: (email: String) -> String?,
    registerAccept: (name: String, email: String, password: String) -> String?,
) {
    val snackBarHostState = remember {
        SnackbarHostState()
    }
    val isLogin = remember {
        mutableStateOf(true)
    }
    val isForgot = remember {
        mutableStateOf(false)
    }
    val coroutineScope = rememberCoroutineScope()
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(colors.background)
    ) {
        Column(
            modifier = modifier.fillMaxWidth()
        ) {
            Spacer(modifier = Modifier.height(16.dp))
            headerContent()
            Spacer(modifier = Modifier.height(16.dp))
            LoginFormTitle(
                isLogin = isLogin.value,
                isForgot = isForgot.value,
                titles = titles,
                colors = colors
            )
            Spacer(modifier = Modifier.height(16.dp))
            LoginFormInputsForm(
                colors = colors,
                isLogin = isLogin.value,
                isForgot = isForgot,
                passwordConfirmError = { error ->
                    coroutineScope.launch {
                        snackBarHostState.showSnackbar(
                            message = error,
                            duration = SnackbarDuration.Indefinite,
                            withDismissAction = true
                        )
                    }
                },
                emailValidate = emailValidate,
                passwordValidate = passwordValidate,
                privacyPolicyOnClick = { policyPrivacyOnClick() },
                termsOfUseOnClick = { termsOfUseOnClick() },
                forgotPasswordAcceptOnClick = { email ->
                    val action = forgotPasswordOnClick(email)
                    if (!action.isNullOrBlank()) {
                        coroutineScope.launch {
                            snackBarHostState.showSnackbar(
                                message = action,
                                duration = SnackbarDuration.Indefinite,
                                withDismissAction = true
                            )
                        }
                    }
                },
                registerAcceptOnClick = { name, email, password ->
                    val action = registerAccept(name, email, password)
                    if (!action.isNullOrBlank()) {
                        coroutineScope.launch {
                            snackBarHostState.showSnackbar(
                                message = action,
                                duration = SnackbarDuration.Indefinite,
                                withDismissAction = true
                            )
                        }
                    }
                },
                loginAcceptOnClick = { email, password ->
                    val action = loginAccept(email, password)
                    if (!action.isNullOrBlank()) {
                        coroutineScope.launch {
                            snackBarHostState.showSnackbar(
                                message = action,
                                duration = SnackbarDuration.Indefinite,
                                withDismissAction = true
                            )
                        }
                    }
                },
                loginDenyOnClick = {
                    loginDeny()
                },
                registerDenyOnClick = {
                    isLogin.value = true
                }
            )
            Spacer(modifier = Modifier.height(16.dp))
            AnimatedVisibility(visible = isLogin.value && !isForgot.value) {
                LoginFormSignUpCard(
                    modifier = signUpCardModifier,
                    colors = colors,
                    titles = titles,
                    internalPadding = signUpCardInternalPadding,
                    elevation = signUpCardElevation
                ) {
                    isLogin.value = false
                }
            }
        }
        SnackbarHost(
            modifier = Modifier.align(Alignment.BottomCenter),
            hostState = snackBarHostState
        )
    }
}

@Preview(showBackground = true)
@Composable
fun LoginFormPreview() {
    LoginForm(
        modifier = Modifier.padding(32.dp),
        headerContent = {
            Row(
                modifier = Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Center
            ) {
                Icon(
                    imageVector = Icons.Rounded.Android,
                    contentDescription = "Android",
                    modifier = Modifier.size(80.dp)
                )
                Text(text = "Logo", style = MaterialTheme.typography.displayMedium)
            }
        },
        registerAccept = { name, email, password ->
            "register"
        },
        forgotPasswordOnClick = {
            "forgot password"
        },
        loginAccept = { email, password ->
            "login"
        },
        loginDeny = {}
    )
}