package br.com.uware.androidstudiobrasiljetpackcompose.widgets.login_form

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp


/**
 * LoginFormInputsForm
 * Inputs for login and register form.
 * @param modifier Modifier for Column.
 * @param titles LoginFormTitles.kt data class config file.
 * @param colors LoginFormColors.kt data class config file.
 * @param isLogin Boolean if is login or register.
 * @param buttonElevation Dp for button elevation.
 * @param passwordConfirmError { String } for password match confirmation.
 * @param registerAcceptOnClick { name, email, password } returns when click in accept.
 * @param registerDenyOnClick { Unit } deny button click.
 * @param loginAcceptOnClick { email, password } returns when click in accept.
 * @param loginDenyOnClick { Unit } deny button click.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 03/06/2022 - Initial release.
 */
@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun LoginFormInputsForm(
    modifier: Modifier = Modifier,
    titles: LoginFormStrings = LoginFormStrings(),
    colors: LoginFormColors = LoginFormColors(),
    isLogin: Boolean = true,
    isForgot: MutableState<Boolean>,
    buttonElevation: Dp = 16.dp,
    passwordConfirmError: (String) -> Unit,
    emailValidate: (email: String) -> Boolean,
    passwordValidate: (password: String) -> Boolean,
    privacyPolicyOnClick: () -> Unit = {},
    termsOfUseOnClick: () -> Unit = {},
    registerAcceptOnClick: (name: String, email: String, password: String) -> Unit,
    registerDenyOnClick: () -> Unit = {},
    forgotPasswordAcceptOnClick: (email: String) -> Unit,
    loginAcceptOnClick: (email: String, password: String) -> Unit,
    loginDenyOnClick: () -> Unit = {},
) {
    val name = remember {
        mutableStateOf("")
    }
    val email = remember {
        mutableStateOf("")
    }
    val password = remember {
        mutableStateOf("")
    }
    val passwordConfirm = remember {
        mutableStateOf("")
    }
    val passwordVisibility = remember {
        mutableStateOf(false)
    }
    val passwordConfirmVisibility = remember {
        mutableStateOf(false)
    }
    val checkedPolitics = remember {
        mutableStateOf(false)
    }
    val misForgot = remember {
        isForgot
    }
    val validInput = remember(email.value, password.value) {
        if (!misForgot.value)
            emailValidate(email.value) &&
                    passwordValidate(password.value)
        else
            emailValidate(email.value)
    }
    val keyboardController = LocalSoftwareKeyboardController.current
    val focusRequesters = List(4) { FocusRequester() }
    Column(
        modifier = modifier.fillMaxWidth()
    ) {
        AnimatedVisibility(visible = !isLogin) {
            LoginFormInputText(
                modifier = Modifier.focusRequester(focusRequesters[0]),
                valueState = name,
                title = titles.name,
                color = colors.color,
                keyboardType = KeyboardType.Text,
                onAction = KeyboardActions {
                    focusRequesters[1].requestFocus()
                }
            )
        }
        LoginFormInputText(
            modifier = Modifier.focusRequester(focusRequesters[1]),
            valueState = email,
            title = titles.email,
            color = colors.color,
            imeAction = if(misForgot.value) ImeAction.Done else ImeAction.Next,
            onAction = KeyboardActions {
                if(misForgot.value) keyboardController?.hide()
                else focusRequesters[2].requestFocus()
            }
        )
        AnimatedVisibility(visible = isLogin && !misForgot.value || !isLogin) {
            LoginFormInputPassword(
                modifier = Modifier.focusRequester(focusRequesters[2]),
                passwordState = password,
                title = titles.password,
                passwordVisibility = passwordVisibility,
                color = colors.color,
                imeAction = if (isLogin) ImeAction.Done else ImeAction.Next,
                onAction = KeyboardActions {
                    if (isLogin) keyboardController?.hide()
                    else focusRequesters[3].requestFocus()
                }
            )
        }
        AnimatedVisibility(visible = isLogin && !misForgot.value) {
            Column(
                modifier = Modifier.fillMaxWidth()
            ) {
                TextButton(
                    modifier = Modifier
                        .padding(top = 8.dp)
                        .align(Alignment.End),
                    onClick = {
                        misForgot.value = true
                    },
                    colors = ButtonDefaults.buttonColors(
                        contentColor = colors.color,
                        containerColor = Color.Transparent
                    )
                ) {
                    Text(
                        modifier = Modifier,
                        text = titles.forgotPassword
                    )
                }
            }
        }
        AnimatedVisibility(visible = !isLogin) {
            LoginFormInputPassword(
                modifier = Modifier.focusRequester(focusRequesters[3]),
                passwordState = passwordConfirm,
                title = titles.passwordConfirm,
                passwordVisibility = passwordConfirmVisibility,
                color = colors.color,
                onAction = KeyboardActions {
                    keyboardController?.hide()
                }
            )
        }
        Spacer(modifier = Modifier.height(16.dp))
        LoginFormChoiceRow(
            isLogin = isLogin,
            isForgot = misForgot.value,
            colors = colors,
            titles = titles,
            elevation = buttonElevation,
            acceptEnabled = validInput,
            denyOnClick = {
                if (misForgot.value) misForgot.value = false
                else if (isLogin) loginDenyOnClick()
                else registerDenyOnClick()
            }
        ) {
            if (misForgot.value) forgotPasswordAcceptOnClick(email.value)
            else if (isLogin) loginAcceptOnClick(email.value, password.value)
            else {
                if (password.value != passwordConfirm.value)
                    passwordConfirmError(titles.passwordConfirmError)
                else if (!checkedPolitics.value)
                    passwordConfirmError(titles.checkedError)
                else registerAcceptOnClick(
                    name.value,
                    email.value,
                    password.value
                )
            }
        }
        AnimatedVisibility(visible = !isLogin) {
            LoginFormCheckBoxRow(
                checked = checkedPolitics,
                colors = colors,
                titles = titles,
                privacyOnClick = {
                    privacyPolicyOnClick()
                },
                termsOnClick = {
                    termsOfUseOnClick()
                }
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun LoginFormInputFormPreview() {
    val isfor = remember {
        mutableStateOf(false)
    }
    LoginFormInputsForm(
        isLogin = true,
        isForgot = isfor,
        forgotPasswordAcceptOnClick = { null },
        loginAcceptOnClick = { email, password ->

        },
        registerAcceptOnClick = { name, email, password ->

        },
        passwordConfirmError = { error ->

        },
        emailValidate = { true },
        passwordValidate = { true }
    )
}