package br.com.uware.androidstudiobrasiljetpackcompose.widgets.login_form

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.AppRegistration
import androidx.compose.material.icons.rounded.Email
import androidx.compose.material.icons.rounded.Login
import androidx.compose.ui.graphics.vector.ImageVector


/**
 * LoginFormIcons
 * Icons in login form.
 * @param login ImageVector for login form.
 * @param register ImageVector for register form.
 * @author Rodrigo Leutz
 * @version 1.0.0 - 03/06/2022 - Initial release.
 */
data class LoginFormIcons(
    var login: ImageVector = Icons.Rounded.Login,
    var register: ImageVector = Icons.Rounded.AppRegistration,
    var forgotPassword: ImageVector = Icons.Rounded.Email
)